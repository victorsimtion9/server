package ch.uzh.ifi.hase.soprafs23.controller;

import ch.uzh.ifi.hase.soprafs23.rest.dto.UserGetDTO;
import ch.uzh.ifi.hase.soprafs23.service.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

/**
 * User Controller
 * This class is responsible for handling all REST request that are related to
 * the user.
 * The controller will receive the request and delegate the execution to the
 * UserService and finally return the result.
 */
@RestController
public class LoginController {

    private final UserService userService;

    LoginController(UserService userService) {
        this.userService = userService;
    } // informeaza te


    @GetMapping("users/login")
    @ResponseStatus(code = HttpStatus.OK, reason = "OK") // informeaza te
    @ResponseBody
    public UserGetDTO loginUser(@RequestBody UserGetDTO userGetDTO) {
        // convert API user to internal representation
        if(userService.verifyUser(userGetDTO.getUsername(), userGetDTO.getPassword()))
        return userGetDTO;
        return null;





        // create user
        // make a query

        // convert internal representation of user back to API

        // return ok ul sau nu

    }
}
